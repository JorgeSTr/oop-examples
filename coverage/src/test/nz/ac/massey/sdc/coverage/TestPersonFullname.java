package test.nz.ac.massey.sdc.coverage;

import static org.junit.Assert.*;

import nz.ac.massey.sdc.coverage.Address;
import nz.ac.massey.sdc.coverage.Gender;
import nz.ac.massey.sdc.coverage.Person;

import org.junit.Test;

public class TestPersonFullname {

	@Test
	public void test1() {
		Person p = new Person("Tim","Smith",Gender.MALE,null,true,22);
		assertEquals("Mr. Tim Smith",p.getFullName());
	}
	
	@Test
	public void test2() {
		Person p = new Person("Kate","Smith",Gender.FEMALE,null,true,22);
		assertEquals("Mrs. Kate Smith",p.getFullName());
	}
	
	@Test
	public void test3() {
		Person p = new Person("Kate","Smith",Gender.FEMALE,null,false,17);
		assertEquals("Miss Kate Smith",p.getFullName());
	}

}
