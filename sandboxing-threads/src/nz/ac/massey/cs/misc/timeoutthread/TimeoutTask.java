package nz.ac.massey.cs.misc.timeoutthread;

import java.util.concurrent.*;

import nz.ac.massey.cs.misc.threats.RunForever;

public class TimeoutTask {

	public static void main(String[] args) throws Exception {
		
		System.out.println("Main thread is " + Thread.currentThread().getName());
		
		Thread.sleep(10000); // some time to start VisualVM
		
		ExecutorService executor = Executors.newFixedThreadPool(1);

		Callable<Boolean> callable = new Callable<Boolean>() {
				@Override
				public Boolean call() throws Exception {
						new RunForever().run();
						return true;
				}
		};
		Future<Boolean> future = executor.submit(callable);
		Thread.sleep(10000);
		future.cancel(true);
		
		System.out.println("I had enough - shoting down evil thread");
		Thread.sleep(5000);
		
		// not that the thread can be reused - lets do this again
		future = executor.submit(callable);
		Thread.sleep(10000);
		future.cancel(true);
		
		System.out.println("shoting down another evil thread");
		System.exit(0);
	}

}
