package nz.ac.massey.cs.misc.securitymanager;

import java.io.FileDescriptor;
import java.util.HashSet;
import java.util.Set;

public class MySecurityManager extends SecurityManager {
	
	private Set<Thread> restrictedThreads = new HashSet<Thread>();
	
	public synchronized void restrict(Thread t) {
		restrictedThreads.add(t);
		System.out.println("restricting thread " + t.getName());
	}
	
	public synchronized void unrestrict(Thread t) {
		if (restrictedThreads.remove(t)) {
			System.out.println("unrestricting thread " + t.getName());
		}
		else {
			System.out.println("could not find thread " + t.getName());
		}
	}
 
//	@Override public synchronized void checkWrite(FileDescriptor f) {
//		if (restrictedThreads.contains(Thread.currentThread())) {
//			System.out.println("Denying file access to " + Thread.currentThread());
//			throw new SecurityException ();
//		}
//		else {
//			System.out.println("Allowing file access to " + Thread.currentThread());
//		}
//	}
	
	@Override public synchronized void checkWrite(String s) {
		if (restrictedThreads.contains(Thread.currentThread())) {
			System.out.println("Denying file access to " + Thread.currentThread());
			throw new SecurityException ();
		}
		else {
			System.out.println("Allowing file access to " + Thread.currentThread());
		}
	}
}
