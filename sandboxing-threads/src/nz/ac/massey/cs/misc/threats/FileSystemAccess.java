package nz.ac.massey.cs.misc.threats;

import java.io.*;

public class FileSystemAccess implements Runnable {

	public FileSystemAccess() {
		super();
	}

	// fields are for testing only
	private boolean success = false;

	public void run() {
		OutputStream out = null;
		try {
			out = new FileOutputStream("tmp.txt");
			out.write(42);
			success = true;
			out.close();
		}  catch (IOException e) {
			e.printStackTrace();
			success = false;
		}
	}

	public boolean isSuccess() {
		return success;
	}

}
