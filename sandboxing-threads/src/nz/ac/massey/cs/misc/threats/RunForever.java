package nz.ac.massey.cs.misc.threats;

public class RunForever implements Runnable {

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("I am alive (thread:  " + Thread.currentThread() + ")");
		}
	}


}
