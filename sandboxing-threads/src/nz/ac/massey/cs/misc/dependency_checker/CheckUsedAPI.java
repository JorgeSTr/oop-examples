package nz.ac.massey.cs.misc.dependency_checker;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.objectweb.asm.ClassReader;

public class CheckUsedAPI {

	public void check(Class clazz) throws Exception {
		DependencyCollector  collector = new DependencyCollector();
		// map clazz to .class file
		File classFile = new File("bin/" + clazz.getName().replace('.','/')+".class");
		InputStream in = new FileInputStream(classFile);
		new ClassReader(in).accept(collector, 0);
		
		for (String ref:collector.getReferenced()) {
			if (ref.startsWith("java.io")) throw new SecurityException("Illegal access to java.io");
		}
        
	}

}
