package nz.ac.massey.cs.misc.dependency_checker;


import nz.ac.massey.cs.misc.threats.FileSystemAccess;
import org.junit.Before;
import org.junit.Test;

public class TestUsedAPIChecker {

	@Before
	public void setUp() throws Exception {
	}

	@Test(expected=SecurityException.class)
	public void test() throws Exception {
		new CheckUsedAPI().check(FileSystemAccess.class);
	}

}
