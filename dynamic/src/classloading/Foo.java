package classloading;

public class Foo {
	static {
		System.out.println("Loaded class " + Foo.class + " using class loader " + Foo.class.getClassLoader());
	}
}
