package classloading;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

public class RunExample {

	public static void main(String[] args) throws Exception {
		
		// use system class loader
		Class clazz1 = Class.forName("classloading.Foo");
		Class clazz2 = Class.forName("classloading.Foo");
		
		System.out.println("clazz1==clazz2 : " + (clazz1==clazz2));
		
		URL[] additionalClassLoaction = {new File("./src2").toURL()};
		ClassLoader loader1 = URLClassLoader.newInstance(additionalClassLoaction,null);
		ClassLoader loader2 = URLClassLoader.newInstance(additionalClassLoaction,null);
		
		clazz1 = loader1.loadClass("Foo2");
		clazz2 = loader2.loadClass("Foo2");
		
		System.out.println("clazz1==clazz2 : " + (clazz1==clazz2));
		
		// the JVM spec 1.7 section 5.5 is a bit vague when the static block must be called - 
		// but one of the events triggering it is instantiation
		clazz1.newInstance();
		clazz2.newInstance();
		
		
	}

}
