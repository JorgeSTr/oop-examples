package reflection;

import java.beans.*;
import java.io.FileInputStream;



/**
 * Example how to load objects with a framework that is based on reflection.
 * @author Jens Dietrich
 * @version 1.0
 */

public class LoadObject {

	/**
	 * Executable main method.
	 * @param runetime parameters
	 */
	public static void main(String[] args) throws Exception {
		XMLDecoder decoder = new XMLDecoder(new FileInputStream("student.xml"));
		Object obj = decoder.readObject();
		decoder.close();
		InspectObject.inspect(obj);
	}


}
