package reflection;

import java.beans.*;
import java.io.FileOutputStream;



/**
 * Example how to store objects with a framework that is based on reflection.
 * @author Jens Dietrich
 * @version 1.0
 */

public class SaveObject {

	/**
	 * Executable main method.
	 * @param runetime parameters
	 */
	public static void main(String[] args) throws Exception {
		Student s = new Student();
		s.setName("Clinton");
		s.setFirstName("Bill");
		XMLEncoder encoder = new XMLEncoder(new FileOutputStream("student.xml"));
		encoder.writeObject(s);
		encoder.close();
		System.out.println("Objects written");
	}


}
