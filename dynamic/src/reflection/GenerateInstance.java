package reflection;
/**
 * Example how to generate instances using reflection.
 * @author Jens Dietrich
 * @version 1.0
 */
public class GenerateInstance {

	/**
	 * Executable main method.
	 * @param runetime parameters
	 */
	public static void main(String[] param) throws Exception {
		String className = "reflection.Student";
		Class clazz = Class.forName(className);
		System.out.println("Class loaded: " + clazz);
		Object instance = clazz.newInstance();
		System.out.println("Instance created: " + instance);
		System.out.println("The instance is of the following type: " + instance.getClass());
	}

}
