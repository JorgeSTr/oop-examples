package reflection;
/**
 * Example how to perform methods dynamically.
 * @author Jens Dietrich
 * @version 1.0
 */
public class PerformMethod {

	/**
	 * Executable main method.
	 * @param runetime parameters
	 */
	public static void main(String[] param) throws Exception {
		Student object = new Student();
		java.lang.reflect.Method method = object.getClass().getMethod("getName",new Class[]{});
		Object returnValue = method.invoke(object,new Object[]{});
		System.out.println("The return value of new Student().getName() is " + returnValue);
	}

}
