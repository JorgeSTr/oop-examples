package osgi.ds.shortdates;

import java.text.DateFormat;
import java.util.Date;

import osgi.ds.servicedefs.DateFormatter;


/**
 * Defines a service as an interface.
 * Put in a separate bundle that can then be referenced by both consumers and providers.
 * @author Jens Dietrich 
 */

public class ShortDateFormatter implements DateFormatter {

	@Override
	public String toString(Date d) {
		return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(d);
	}

}
