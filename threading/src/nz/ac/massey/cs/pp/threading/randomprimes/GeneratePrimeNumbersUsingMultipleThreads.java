package nz.ac.massey.cs.pp.threading.randomprimes;

import java.util.Vector;
import java.util.List;

/**
 * Simple multi-threaded programs to compute some big prime numbers.
 * @author jens dietrich
 */
public class GeneratePrimeNumbersUsingMultipleThreads {

	public static void main(String[] args) throws InterruptedException {      
		// note that Vector is a "thread-safe" version of ArrayList
		List<Integer> primeNumbers = new Vector<Integer>();
		
		for (int i=0;i<Settings.WORKER_COUNT;i++) {
			GeneratePrimeNumbersWorker worker = new GeneratePrimeNumbersWorker(primeNumbers,"worker"+i);
			new Thread(worker).start();
		}

	}


}
