package nz.ac.massey.cs.pp.threading.randomprimes;

import java.util.Random;

/**
 * A very simple (and not very clever) prime number generator.
 * Real world applications would use APIs like
 * java.math.BigIntereger.nextProbablePrime()
 * see http://docs.oracle.com/javase/7/docs/api/java/math/BigInteger.html#nextProbablePrime() 
 * @author jens dietrich
 */
public class RandomPrimeNumberGenerator {
	
	private static Random random = new Random();
	
	public static int getNextRandomPrime(int maxNumber) {
		int n = -1; 
		while (n==-1 || !isPrime(n)) {
			n = random.nextInt(maxNumber);
		}
		return n;
	}
	
	private static boolean isPrime(int n) {
		for(int i=2; i < n ; i++){
            if(n % i == 0){
                return false;
            }
		}
		return true;
	}

}
