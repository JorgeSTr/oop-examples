package nz.ac.massey.cs.pp.threading.randomprimes;
/**
 * Settings can be configured here.
 * @author jens dietrich
 */
public interface Settings {
	// the number of random numbers to be generated
	public final static int COUNT = 10;
	// the max number used to generate random prime numbers
	public final static int MAX_NUMBER = 1_000_000_000;
	// number of workers
	public final static int WORKER_COUNT = 2;
}
