# interception example - inject code AOP like
 
require 'person'

# hack object, add additional instance variable
class Object
  def timestamp
    return @timestamp
  end
  def timestamp=(aTime)
    @timestamp = aTime
  end
end
# hack class, inject code into constructors
# not alias used
class Class
  alias_method :old_new,  :new
  def new(*args)
    result = old_new(*args)
    result.timestamp = Time.now
    return result
  end
end
p = Person.new(42)
sleep 3
puts p.timestamp
puts Time.now

