package nz.ac.massey.pp.javamem.gc;

/**
 * Example showing the it is possible to keep on creating objects,
 * the JVM will automatically free memory via garbage collection. 
 * @author jens dietrich
 */
public class CreateInfinitelyManyObjects1 {

	public static void main(String[] args) throws Exception {
		while (true) {
		    for (int i=0;i<1000000;i++) {
		    	Object obj = new Object();
		    }
		    
		    // break between, allows JVM to clean up (GC !) 
		    Thread.sleep(100);
		}
	}
}
