package lsp;

public class RunExample {
	public static void main(String args[]) {
		// works
		EvenNumberChecker a = new EvenNumberChecker();
		System.out.println(a.isEvenNumber(-42));

		// illegal substitution violating Liskov's substitution principle:
		// compiles but fails at runtime
		a = new SafeEvenNumberChecker();
		System.out.println(a.isEvenNumber(-42));
	}

}
