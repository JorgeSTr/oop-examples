package contracts.hashequals;

/**
 * A correct and efficient implementation of hashCode is implemented here
 * @author Jens Dietrich 
 * @version 1.0
 */

public class Point2D_3 extends Point2D {

	public Point2D_3(int x, int y) {
		super(x, y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	

}
