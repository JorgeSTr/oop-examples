package contracts.hashequals;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

/**
 * Tests the different Point2D classes. An association is added, and then we try to find
 * the association using an equal (but different w.r.t. ==) key. This only works if 
 * hashCode is implemented correctly in the respective classes.
 * @author Jens Dietrich 
 * @version 2.0 - now uses junit4
 */

public class HashMapTests {
	
	@Test
	public void test4Point2D_1() {
		Map<Point2D_1,String> map = new HashMap<Point2D_1,String>();
		map.put(new Point2D_1(42,42),"test");
		// this will fail!
		assertEquals("test",map.get(new Point2D_1(42,42)));
	}
	@Test
	public void test4Point2D_2() {
		Map<Point2D_2,String> map = new HashMap<Point2D_2,String>();
		map.put(new Point2D_2(42,42),"test");
		assertEquals("test",map.get(new Point2D_2(42,42)));
	}
	@Test
	public void test4Point2D_3() {
		Map<Point2D_3,String> map = new HashMap<Point2D_3,String>();
		map.put(new Point2D_3(42,42),"test");
		assertEquals("test",map.get(new Point2D_3(42,42)));
	}

}
