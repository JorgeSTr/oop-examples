package contracts.hashequals;

/**
 * Class that wrong uses an incorrect hash code implementation that breaks the contract with equals
 * - we can now have equal objects with different hash codes.
 * System.identityHashCode will provide different values for different (w.r.t. ==) objects, even if they are equal!
 * Therefore, maps don't work as expected. See also HashMapTests#test4Point2D_1()  for a test case that fails.
 * @author Jens Dietrich 
 * @version 1.0
 */
public class Point2D_1 extends Point2D {

	public Point2D_1(int x, int y) {
		super(x, y);
	}

	@Override
	public int hashCode() {
		return System.identityHashCode(this); // refers to object identity - as in Object
	}
	

}
