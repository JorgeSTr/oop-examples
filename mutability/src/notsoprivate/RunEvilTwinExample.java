package notsoprivate;

public class RunEvilTwinExample {

	public static void main(String[] args) {
		EvilTwin a = new EvilTwin(42);
		System.out.println("a.x = " + a.getX());
		EvilTwin b = a.clone();
		b.setX(43);
		System.out.println("a.x = " + a.getX());
	}

}
