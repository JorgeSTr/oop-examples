package notsoprivate;

public class EvilTwin {
	private int x = 0;
	private EvilTwin master = null;
	
	public EvilTwin(int x) {
		super();
		this.x = x;
	} 
	
	
	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
		// this is the critical code: this will also change the 
		// value of a private field of another object
		if (this.master!=null) {
			this.master.x = x;
		}
	}

	
	public EvilTwin clone() {
		EvilTwin clone = new EvilTwin(this.x);
		clone.master = this;
		return clone;
	}
	
}
