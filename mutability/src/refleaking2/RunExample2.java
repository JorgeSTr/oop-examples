package refleaking2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RunExample2 {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		EvilEvenNumberCounter.countEvenNumbers(Collections.unmodifiableList(list));
		System.out.println("list size is " + list.size());
	}

}
