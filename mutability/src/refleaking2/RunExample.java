package refleaking2;

import java.util.ArrayList;
import java.util.List;

public class RunExample {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		EvilEvenNumberCounter.countEvenNumbers(list);
		System.out.println("list size is " + list.size());
	}

}
