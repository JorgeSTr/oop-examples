

package nz.ac.massey.webtech.servlets.xml;

import javax.servlet.*;
import javax.servlet.http.*;


/**
 * Simple servlet dispatching the request in order to invoke the appropriate servlet.
 * (i.e., the servlet producing the appropriate content type). 
 * @author  Jens Dietrich   
 * @version 1.0
 */
 public class CatalogServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
       String userAgent = request.getHeader("user-agent");
       
       RequestDispatcher rd = null; 
       
       if (userAgent.indexOf("Opera")>-1) rd = getServletContext().getRequestDispatcher("/catalog.wml");
       else rd = getServletContext().getRequestDispatcher("/catalog.html");
       
       rd.forward(request,response);
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
