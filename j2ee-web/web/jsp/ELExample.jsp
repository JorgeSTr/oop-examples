<%@page contentType="text/html"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=MacRoman">
        <title>Shows the use of the JSP 2.0 Expression Language</title>
    </head>


    <body>
        <h1>JSP-EL Examples</h1>
        <p>Basic math in JSP-EL: 3+4 is ${3+4}</p>

        <form>
            <input type='text' name='Name' value="${param.Name}"/>
            <input type='text' name='FirstName'value="${param.FirstName}" />
            <input type='submit'/>
        </form>

    </body>
</html>
