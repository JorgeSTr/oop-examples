<%@page contentType="text/html"%>
<html>
<head><title>Student Editor</title></head>
<body>

<jsp:useBean id="student" scope="session" class="nz.ac.massey.webtech.jsp.Student" /> 

<!-- process form -->
<jsp:setProperty name="student" property="name" param="name"/>
<jsp:setProperty name="student" property="firstName" param="fname"/>
<jsp:setProperty name="student" property="age" param="age"/>

<!-- header -->
<h3>Edit details of Mr./Mrs <jsp:getProperty name="student"  property="name" /></h3>

<!-- print form -->
<form>
name:<br>
<input type="text" name="name" size="40" value="<jsp:getProperty name="student"  property="name" />">
<p>
first name:<br>
<input type="text" name="fname" size="40" value="<jsp:getProperty name="student"  property="firstName" />">
<p>
age:<br>
<input type="text" name="age" size="40" value="<jsp:getProperty name="student"  property="age" />">
<p>
<input type="submit" value="Apply">
</form>


</body>
</html>
