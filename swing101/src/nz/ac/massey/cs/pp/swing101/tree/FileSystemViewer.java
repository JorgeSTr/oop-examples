package nz.ac.massey.cs.pp.swing101.tree;


import javax.swing.*;

public class FileSystemViewer extends JFrame {

	JTree tree = new JTree();

	public static void main(String[] args) {
		FileSystemViewer win = new FileSystemViewer();
		win.setVisible(true);
	}

	public FileSystemViewer() {
		super();
		init();
	}

	private void init() {
		this.getContentPane().add(new JScrollPane(tree));
		tree.setBorder(BorderFactory.createEtchedBorder());
		this.setLocation(100,100);
		this.setSize(300,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tree.setModel(new FileSystemTreeModel());
	}

}
