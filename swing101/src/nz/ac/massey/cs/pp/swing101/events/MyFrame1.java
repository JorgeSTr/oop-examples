package nz.ac.massey.cs.pp.swing101.events;

import java.awt.FlowLayout;
import javax.swing.*;
/**
 * No event handling here, but note that the components are referenced by instance variables.
 */
public class MyFrame1 extends JFrame  {
	
	private JButton exitButton = new JButton("exit");
	private JButton saveButton = new JButton("save");
	private JButton loadButton = new JButton("load");
	
	public MyFrame1() {
		super();
		init();
	}
	
	private void init() {
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(exitButton);
		this.getContentPane().add(loadButton);
		this.getContentPane().add(saveButton);
		this.setLocation(100,100);
		this.setSize(300,200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		MyFrame1 frame = new MyFrame1();
		frame.setVisible(true);
	}


}
