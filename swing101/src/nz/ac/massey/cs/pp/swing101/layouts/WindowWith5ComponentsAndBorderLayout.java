package nz.ac.massey.cs.pp.swing101.layouts;


import java.awt.BorderLayout;
import javax.swing.*;

public class WindowWith5ComponentsAndBorderLayout {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setLocation(100, 100);
		frame.setSize(300,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(10,10));
		addComponents(frame);
		frame.setVisible(true);
	}

	private static void addComponents(JFrame frame) {
		JButton button1 = new JButton("button 1");
		frame.getContentPane().add(button1,BorderLayout.NORTH);
		
		JButton button2 = new JButton("button 2");
		frame.getContentPane().add(button2,BorderLayout.EAST);
		
		JButton button3 = new JButton("button 3");
		frame.getContentPane().add(button3,BorderLayout.SOUTH);
		
		JButton button4 = new JButton("button 4");
		frame.getContentPane().add(button4,BorderLayout.WEST);
		
		JButton button5 = new JButton("button 5");
		frame.getContentPane().add(button5,BorderLayout.CENTER);
	}

}
