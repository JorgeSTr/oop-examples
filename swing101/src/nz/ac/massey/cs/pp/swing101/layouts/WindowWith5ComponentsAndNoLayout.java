package nz.ac.massey.cs.pp.swing101.layouts;

import javax.swing.*;

public class WindowWith5ComponentsAndNoLayout {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setLocation(100, 100);
		frame.setSize(300,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponents(frame);
		frame.setVisible(true);
	}

	private static void addComponents(JFrame frame) {
		JButton button1 = new JButton("button 1");
		frame.getContentPane().add(button1);
		
		JButton button2 = new JButton("button 2");
		frame.getContentPane().add(button2);
		
		JButton button3 = new JButton("button 3");
		frame.getContentPane().add(button3);
		
		JButton button4 = new JButton("button 4");
		frame.getContentPane().add(button4);
		
		JButton button5 = new JButton("button 5");
		frame.getContentPane().add(button5);
	}

}
