
package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Simple servlet filter - intercepts requests and logs the response time.
 * @author jens dietrich
 */
public class Profiler implements Filter {

    private ServletContext context = null;

    public Profiler() {
    }


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {



        // before processing
        long timeBefore = System.currentTimeMillis();

        // actual request (or further filters .. who knows)
        chain.doFilter(request, response);

        // after processing
        long timeAfter = System.currentTimeMillis();
        
        context.log("responded to request in " + (timeAfter-timeBefore) + "ms");


    }

    public void init(FilterConfig filterConfig) throws ServletException {
        this.context = filterConfig.getServletContext();
    }

    public void destroy() {}
}
