package nz.ac.massey.cs.asdc.patterns.xml.stax;

import java.io.FileInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

public class RunExample {

	public static void main(String[] args) throws Exception {
		XMLInputFactory xmlif = XMLInputFactory.newInstance();

		XMLStreamReader xmlr = xmlif.createXMLStreamReader(new FileInputStream("email1.xml"));
		
        //check if there are more events in the input stream
        boolean toMode = false;
        while(xmlr.hasNext()){
             
            int type = xmlr.next();
            String tagName = xmlr.getLocalName();
            if (type==XMLEvent.START_ELEMENT && tagName.equals("to")) {
            	toMode=true;
            }
            else if (type==XMLEvent.END_ELEMENT && tagName.equals("to")) {
            	toMode=false;
            	System.out.println();
            }
            else if (type==XMLEvent.START_ELEMENT && toMode && tagName.equals("display_name")) {
            	System.out.print(xmlr.getElementText());
            	System.out.print(" ");
            }
            else if (type==XMLEvent.START_ELEMENT && toMode && tagName.equals("email_address")) {
            	System.out.print("<");
            	System.out.print(xmlr.getElementText());
            	System.out.print(">");
            }
            
        }

	}
	

}
