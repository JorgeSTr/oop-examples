# mixin example using the standard Comparable mixin
require 'person'
p1 = Person.new(1)
p2 = Person.new(2)
p1.firstName = "John"
p2.firstName = "Tom"
# by defining <=> we get < and <= etc for free through the mixin
puts p1<p2
puts p1<=p2
puts p1>p2
