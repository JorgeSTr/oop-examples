class Person
  include Comparable
  def <=>(other)
    self.firstName <=> other.firstName
  end
  def initialize(pid)
    @name=""
    @firstName=""
    @pid=pid
  end
  def fullName 
    return self.pid.to_s + ":" + self.firstName + " " + self.name
  end
  # meta programming is used to define setters are getters
  attr_accessor :name, :firstName 
  attr_reader :pid
end
