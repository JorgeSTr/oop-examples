require "test/unit"
require "sudoku"
require "sudoku_reader"

class SudokuTests < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup

  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown

  end

  # Fake test
  def test_reader
    sudoku = SudokuReader.new.read('./data/sudoku1.txt');
    assert_equal(9,sudoku.get(1,1).value)
    assert_equal(6,sudoku.get(9,4).value)
    assert_equal(3,sudoku.get(2,9).value)
  end

   def test_sudoku_row_access
    sudoku = SudokuReader.new.read('./data/sudoku1.txt');
    row1_values = sudoku.cellsInRow(1).collect{ |c| c.value}
    assert_equal([9,0,0,3,5,0,0,6,0],row1_values)

    row9_values = sudoku.cellsInRow(9).collect{ |c| c.value}
    assert_equal([0,3,0,0,4,5,0,0,9],row9_values)
   end

    def test_sudoku_column_access
      sudoku = SudokuReader.new.read('./data/sudoku1.txt');
      col1_values = sudoku.cellsInColumn(1).collect{ |c| c.value}
      assert_equal([9,0,0,3,0,8,4,0,0],col1_values)

      col9_values = sudoku.cellsInColumn(9).collect{ |c| c.value}
      assert_equal([0,0,3,6,0,4,0,0,9],col9_values)
    end

    def test_sudoku_group_access
      sudoku = SudokuReader.new.read('./data/sudoku1.txt');
      col1_values = sudoku.cellsInGroup(2,2).collect{ |c| c.value}
      assert_equal([9,0,0,0,0,0,0,0,7],col1_values)

    end

    def test_possible_values
      sudoku = SudokuReader.new.read('./data/sudoku1.txt');
      values = sudoku.possibleValues(2,1)
      assert_equal([1,2,4,8],values)

      values = sudoku.possibleValues(5,5)
      assert_equal([1,3,6,8,9],values)
    end

end