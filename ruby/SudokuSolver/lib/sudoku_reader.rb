# To change this template, choose Tools | Templates
# and open the template in the editor.
require 'sudoku'
class SudokuReader
  def initialize

  end
  def read(file)
    cells = Array.new
    lines = IO.readlines(file)
    row=1
    col=1
    for line in lines
      col=1
      for token in line.split(',')
        cells.push(Cell.new(col, row,token.to_i))
        col=col+1
      end
      row = row+1
    end
    sudoku = Sudoku.new
    sudoku.cells = cells
    # make the sudoku immutable - note that this is 'shallow', we must do it separately for the cells
    # after this, the individual cells will still be mutable
    sudoku.freeze
    sudoku.cells.freeze


    sudoku
  end
end
