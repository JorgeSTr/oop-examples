# 
# To change this template, choose Tools | Templates
# and open the template in the editor.
 
require 'person'
class Student < Person
  protected
  def initialize(sid,name,firstName)
    @name = name
    @firstName = firstName
    @sid = sid
  end
  public
  def sid
    return @sid
  end
  # overridden
  def fullName 
    return self.sid.to_s + ": " + self.firstName + " " + self.name
  end
    #setters
  def sid=(n)
    @sid = n
  end
end
