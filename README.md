This is a collection of examples I have developed to teach object-oriented concepts, such as Liskovs substitution principle, design by contract, component design (osgi/eclipse), single dispatch, prototyping, contracts in programs, design patterns, dependency injection, dynamic components, aop, meta-programming and web and distributed application design. The example have been developed to teach several 200-400 software engineering courses at Massey University (since 2003) and the University of Canterbury (S1 2012).  [Jens Dietrich](https://sites.google.com/site/jensdietrich/)

There will be no releases, please download the examples by cloning the repository. [Install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) on your computer, and then run the following command from the command line: **git clone https://jensdietrich@bitbucket.org/jensdietrich/oop-examples.git**. Or use a ui application like [SourceTree](https://www.sourcetreeapp.com/).

Overview
--------


structure: folder / programming language / description . Each folder can be checked out as an Eclipse project unless noted otherwise (there are a few NetBeans projects as well). 

Language Features
-----------------

| folder     | lang. | description |
|------------|------------------|-------------|
|**category:**||**Language Features**|
| dispatch | java | shows how method look up does (not) work in java | 
| semantics| java | shows different ways how to deal with the semantics of methods: LSP, semantics by interpolation using junit, and contracts between methods like equals/hashcode | 
| dynamic | java | examples related to reflection, custom classloading and the use of dynamic proxies |
| ruby | ruby | Several example showing the dynamic and meta programming features in ruby, including: dynamically adding  features to both Object and Class (*RubyInterceptors*), (2) continuations (*RubyContinuation*), (3) dynamic proxies (*RubyDoesNotUnderstand*), (4) context-dependent methods, similar to `thisContext` in `Smalltalk` (*RubyStack*). There are several other examples showing basic OO, Java intergation and functional features. These projects can all be checked out as a NetBeans projects! Note that NB does not have ruby support on board anymore, extra plugins must be installed. Most examples do depend on a particular Ruby interpreter or version.| 
| javamemory | java | Examples illustrating how garbage collections and object lifecycle management work in Java |
| threading | java | A very simple example (generating large random prime numbers) illustrating how threading works |
| listcomprehensions | java | Example showing how collections are processed. Currently there are three implementations: classical procedural code, and two more functional implementations using Google Guava with explicit functions and predicate, one using eager and one using lazy evaluation. Another version based on Java8 functional features will be added later |
|**category:**||**Design**|
| designpatterns2 | java | develops a scenario where several design patterns are used: composite, visitor, hierarchical visitor |
| designpatterns3 | java | shows the use of patterns used in Java XML APIs: composite (DOM), observer (SAX), domain specific language aka little language aka interpreter (XPath), interator (STAX) |
| jaxb | java, xml | Example how to generate an API from an XML Schema in order to avoid imposed structural duplication|
| log4j | java | Some examples how to use log4j. log4j is a good library to explain separation of concerns and orthogonality - aspects such as configurators, loggers, levels, appenders and layouts are _orthogonal_. There is also a good example were this fails: the use of layouts in JDBCAppender. See also: [Orthogonality by Example](http://www.javaworld.com/javaworld/jw-05-2013/130501-jtip-orthogonality-by-example.html)|
| java2uml | java | The project contains the log4j sources and a javadoc parameter file uml.config that can be used to reverse engineer a UML (class) model from the log4j sources. The excellent [yDoc](http://www.yworks.com/en/products_ydoc.html) doclet is used for this purpose. The generated model is in the repository, in the doc/uml folder. To recreate the model, just re-run javadoc with the uml.config parameter file. Note that uml.config references yDoc in the tools folder, but yDoc is (for licensing reasons) not in the repository. Users should download the free yDoc community edition and unzip it into the tools folder before running javadoc. |
|**category:**||**Modularity**|
| osgi.raw.* | java | An example for component-based software engineering and using OSGi without any service extensions. Each project has build scripts to generate bundles, these bundles can be deployed on a standard OSGi server, I use knopflerfish for this purpose in my lectures.|
| osgi.ds.* | java | An example for component-based software engineering and dependency injection using OSGi declarative services. This needs to be executed in Eclipse as the example provides a service for the (Equinox) console - it adds a  *today* command. This service is a consumer of the `DateFormatter` service provided by the `LongDates` and `ShortDates` components. To test the component, start/remove the respective bundles (`osgi.ds.LongDates` and `osgi.ds.ShortDates`) from the OSGi console.|
| filters | java | Shows how to use servlet filters as an example of _AOP without AOP_, also shows the use of proxy and chain of responsibility. This can be checked out as a NetBeans project!|
| aop | java,aspectj | some simple aop examples (using aspectj) - in particular the example show how to use point cuts |
|**category:**||**Testing**|
| junit-primer | java |simple junit example using the NZ IRD income tax calculator |
| gui-test | java |shows how (swing) frameworks can be tests automatically using [java.awt.Robot](http://docs.oracle.com/javase/1.5.0/docs/api/java/awt/Robot.html). This is fairly low level, in production, testing frameworks like [Abbot](http://abbot.sourceforge.net/) should be used instead |
|blackbox-test |java|tests a server from the outside (find out whether google knows the _answer to life the universe and everything_). This test is rather low-level, it only uses a http client library. Frameworks like [selenium](http://www.seleniumhq.org/) and [jwebunit](http://jwebunit.sourceforge.net/) could be used to do this in production environments.|
|whitebox-test|java|uses a mock framework to _fake_ a J2EE web server and test a servlet directly|    
|**category:**||**Productivity**|
| templating | java | A simple example that shows the use of a template engine for web page, report of code generation. [http://velocity.apache.org/ Velocity] is used in this example. |
|**category:**||**Security**|
| sandboxing-threads | java | How to sandbox Java threads. Background: in my 2013 software engineering capstone project, students had to implement a web-based IDE for users to create bots playing a simple mathematical game. Compiling and executing submitted code on the server is an open invitation for injection attacks. These examples show several options how to deal with this situation. |
|**category:**||**Web**|
| J2EE | java | Basic servlet / jsp examples, including aspects such as session tracking, cookie handling, generation of binary content, connecting to databases, header and form analysis, filters, modular design with forward and include. This is a NetBeans 7.3 project.|
| ajax | java/javascript | A simple ajax scenario. Contains two clients - an ugly one using pure javascript, and a nicer one that uses the JQuery Ajax API. This is a NetBeans 7.3 project. |
| SSEPerformanceSpike | java | An experiment to compare different implementation strategies for server-sent events using Java servlets. The main aspects compared is the number of reconnects necessary, and the utilisation of threads from the web server thread pool. This is a NetBeans 7.3 project. |
|javascript | javascript | some (older) javascript examples. This is just a plain folder (not IDE project), no web server is needed to execute the scripts|
|**category:**||**Design Case Studies**|
| chat | java,javascript,css,jquery| A web application using server push with server-sent events and inter-session communication.  This code can be checked out as a NetBeans 7 project, and/or build with ANT ([slides](http://tinyurl.com/massey-sse)).| 
| studenteditor | java,javascript,jquery,css,mongodb | A modern one page web application that uses a client side Model-View-Controller (MVC) design. Model and views are bound using [knockout.js](http://knockoutjs.com/). Persistency is provided through a set of RESTful services implemented as servlets, and mapped to a MongoDB database. This code can be checked out as a NetBeans 7 project, and/or build with ANT.  The server is unit tested using standard JUnit tests and the Apache HTTP client.| [http://tinyurl.com/massey-knockout slides]| 

Feel free to reuse and add, please give me (and other contributors) credit.