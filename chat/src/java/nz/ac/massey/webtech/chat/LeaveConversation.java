
package nz.ac.massey.webtech.chat;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Leave a conversation. This is a controller, instead of creating content the request is forwarded.
 * @author jens dietrich
 */
public class LeaveConversation extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
        
         // delegate request back to join
        this.getServletContext().getRequestDispatcher("/join.html").forward(request, response);
    }


    @Override
    public String getServletInfo() {
        return "leave conversation";
    }
}
