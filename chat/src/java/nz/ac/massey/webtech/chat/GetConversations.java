/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.massey.webtech.chat;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Get a list of conversations.
 * @author jens dietrich
 */
public class GetConversations extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // ConversationManager.joinConversation(request.getSession(),"dummy 1","dummy");
        // TODO - remove this is for testing only
   
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            for (String conversation:ConversationManager.getConversations()) {
                out.print(conversation);
                out.print("<br/>");
            }
            
        } finally {            
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Get active conversations";
    }
}
