
package nz.ac.massey.webtech.chat;

/**
 * Simple class representing a simple message.
 * @author jens dietrich
 */
public class Message {
    
    public Message(String user,String text) {
        super();
        this.user = user;
        this.text = text;
    }
    
    private String user = null;
    private String text = null;

    public String getUser() {
        return user;
    }

    public String getText() {
        return text;
    }
    
    public String toJSON() {
        return new StringBuilder()
            .append('{')
            .append("\"user\":\"").append(this.getUser()).append("\"")
            .append(',')
            .append("\"text\":\"").append(this.getText()).append("\"")
            .append('}')
            .toString();
    }


    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if ((this.user == null) ? (other.user != null) : !this.user.equals(other.user)) {
            return false;
        }
        if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
            return false;
        }
        return true;
    }
    
}
