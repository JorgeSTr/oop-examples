package nz.ac.massey.webtech.chat;

import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Post a message.
 * @author jens dietrich
 */
public class PostMessage extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        String message = reader.readLine();
        reader.close();
        ConversationManager.postMessage(request.getSession(), message);
    } 

    @Override
    public String getServletInfo() {
        return "post a message";
    }
}
