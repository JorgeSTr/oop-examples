package test.nz.ac.massey.sdc.dummytest;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The most basic test - to explain the three different possible outcomes when running tests. 
 * @author jens dietrich
 */
public class HelloWorldTest {

	@Test
	public void testWithSuccess() {
		assertTrue(true);
	}

	@Test
	public void testWithAssertionFailure() {
		assertTrue(false);
	}
	
	@Test
	public void testWithRuntimeException() {
		Object obj = null;
		// this will cause a runtime exception
		obj.toString();
	}
	
	@Test
	public void testWithException() throws Exception {
		throw new java.io.IOException();
	}
	
}
