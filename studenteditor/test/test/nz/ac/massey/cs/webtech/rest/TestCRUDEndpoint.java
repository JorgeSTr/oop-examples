
package test.nz.ac.massey.cs.webtech.rest;

import com.mongodb.*;
import java.io.IOException;
import java.net.URI;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.DefaultedHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import static org.junit.Assert.*;
import org.junit.*;

/**
 * A simple set of tests for CRUDEndpoint.
 * @author jens dietrich
 */
public class TestCRUDEndpoint {
    
    public static final String DB_NAME = "mydb";
    public static final String URL = "http://localhost:8080/studenteditor/crud/Student/";
    
    private DB db = null;
    private static DefaultHttpClient httpClient = null;
    
    @Before
    public void setUp() throws Exception {
        MongoClient mongoClient = new MongoClient();
        db = mongoClient.getDB(DB_NAME);
        httpClient = new DefaultHttpClient();
        
        DBCollection coll = db.getCollection("Student");
        coll.remove(new BasicDBObject());
    }
    
    @After
    public void tearDown() {
        // remove all
        DBCollection coll = db.getCollection("Student");
        coll.remove(new BasicDBObject());
        // db.dropDatabase();
        db = null;
        httpClient = null;
    }
    
    @Test 
    public void testCreate() throws Exception {
        // setup request
        HttpResponse response = createPersistentObjectWithPost("Tom","Taylor",24);             
       
        // analyse response
        assertTrue(response.getEntity().getContentType().getValue().startsWith("application/json"));
        JSONObject result = parseJSONPayloadOne(response);
        assertEquals("Taylor",result.get("name"));
        assertEquals("Tom",result.get("firstName"));
        assertNotNull(result.get("id"));
        
        // check db
        DBCollection coll = db.getCollection("Student");
        assertEquals(1,coll.count()); // 1 record
        DBObject record = coll.findOne();
        assertEquals("Taylor",record.get("name"));
        assertEquals("Tom",record.get("firstName"));
        
        System.out.println(result);
    }
    
    @Test 
    public void testUpdate() throws Exception {
        // must post record first 
        HttpResponse response = createPersistentObjectWithPost("Tom","Taylor",24);             
        JSONObject result = parseJSONPayloadOne(response);
        String objectId = (String) result.get("id");
        
        // setup request
        URI uri = new URI(URL+objectId);
        HttpPut request = new HttpPut(uri);
        JSONObject json = new JSONObject();
        json.put("age",47).put("name","Taylor").put("firstName","Tim").put("id",objectId);
        HttpEntity entity = new StringEntity (json.toString(),ContentType.create("application/json", "UTF-8"));
        request.setEntity(entity);
        
        // execute request
        response = httpClient.execute(request);
        
        // analyse response
        StatusLine status = response.getStatusLine();
        assertEquals(HttpServletResponse.SC_OK,status.getStatusCode());
        
        // check db - first name must have been updated
        DBCollection coll = db.getCollection("Student");
        assertEquals(1,coll.count()); // 1 record
        DBObject record = coll.findOne();
        assertEquals("Taylor",record.get("name"));
        assertEquals("Tim",record.get("firstName"));
        
        System.out.println(result);
    }
    
    @Test 
    public void testDelete() throws Exception {
        // must post record first 
        HttpResponse response = createPersistentObjectWithPost("Jens","Dietrich",47);             
        JSONObject result = parseJSONPayloadOne(response);
        String objectId = (String) result.get("id");
        
        // setup request
        URI uri = new URI(URL+objectId);
        HttpDelete request = new HttpDelete(uri);
        
        // execute request
        response = httpClient.execute(request);
        
        // analyse response
        StatusLine status = response.getStatusLine();
        assertEquals(HttpServletResponse.SC_OK,status.getStatusCode());
        
        // check db - first name must have been updated
        DBCollection coll = db.getCollection("Student");
        assertEquals(0,coll.count()); // 0 records
        
        System.out.println(result);
    }
    
    @Test 
    public void testGetOne() throws Exception {
        // must post record first 
        HttpResponse response = createPersistentObjectWithPost("Tom","Taylor",47);             
        JSONObject result = parseJSONPayloadOne(response);
        String objectId = (String) result.get("id");
        
        // setup request
        URI uri = new URI(URL+objectId);
        HttpGet request = new HttpGet(uri);
        
        // execute request
        response = httpClient.execute(request);
        
        // analyse response
        assertTrue(response.getEntity().getContentType().getValue().startsWith("application/json"));
        StatusLine status = response.getStatusLine();
        assertEquals(HttpServletResponse.SC_OK,status.getStatusCode());
        result = parseJSONPayloadOne(response);
        assertEquals("Taylor",result.get("name"));
        assertEquals("Tom",result.get("firstName"));
        assertNotNull(result.get("id"));
        
        System.out.println(result);
    }
    
    
    @Test 
    public void testGetAll() throws Exception {
        // must post record first 
        createPersistentObjectWithPost("Ken","Clark",24).getEntity().consumeContent();
        createPersistentObjectWithPost("Tom","Taylor",18).getEntity().consumeContent();
        createPersistentObjectWithPost("James","Smith",32).getEntity().consumeContent();
    
        // setup request
        URI uri = new URI(URL);
        HttpGet request = new HttpGet(uri);
        
        // execute request
        HttpResponse response = httpClient.execute(request);
        
        // analyse response
        assertTrue(response.getEntity().getContentType().getValue().startsWith("application/json"));
        StatusLine status = response.getStatusLine();
        assertEquals(HttpServletResponse.SC_OK,status.getStatusCode());
        JSONArray result = parseJSONPayloadMany(response);
        assertEquals(3,result.length());
        
        System.out.println(result);
    }
    
    @Test 
    public void testGetSome1() throws Exception {
        // must post record first 
        createPersistentObjectWithPost("Ken","Clark",47).getEntity().consumeContent();
        createPersistentObjectWithPost("Tom","Taylor",18).getEntity().consumeContent();
        createPersistentObjectWithPost("James","Smith",32).getEntity().consumeContent();
    
        // setup request
        URI uri = new URI(URL);
        HttpGet request = new HttpGet(uri);
        //HttpParams params = new DefaultedHttpParams();
        //request.setParams(params);
        
        // execute request
        HttpResponse response = httpClient.execute(request);
        
        // analyse response
        assertTrue(response.getEntity().getContentType().getValue().startsWith("application/json"));
        StatusLine status = response.getStatusLine();
        assertEquals(HttpServletResponse.SC_OK,status.getStatusCode());
        JSONArray result = parseJSONPayloadMany(response);
        assertEquals(3,result.length());
        
        System.out.println(result);
    }
    
    // create a new object using a post request, and return the JSON data returned by the server
    private HttpResponse createPersistentObjectWithPost(String firstName,String name,int age) throws Exception {
        URI uri = new URI(URL);
        HttpPost post = new HttpPost(uri);
        JSONObject json = new JSONObject();
        json.put("age",age).put("name",name).put("firstName",firstName);
        HttpEntity entity = new StringEntity (json.toString(),ContentType.create("application/json", "UTF-8"));
        post.setEntity(entity);
        HttpResponse response =  httpClient.execute(post);
        
        return response;
    }
    
    private JSONObject parseJSONPayloadOne(HttpResponse response) throws IOException, JSONException {
        JSONTokener tokener = new JSONTokener(EntityUtils.toString(response.getEntity()));
        return new JSONObject(tokener);
    }
    
    private JSONArray parseJSONPayloadMany(HttpResponse response) throws IOException, JSONException {
        JSONTokener tokener = new JSONTokener(EntityUtils.toString(response.getEntity()));
        return new JSONArray(tokener);
    }

}
