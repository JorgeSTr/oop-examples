package nz.ac.massey.cs.sdc.log4j;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Simple log application.
 * @author jens dietrich
 *
 */
public class LogItFileAppender {

	public static void main(String[] args) throws Exception {
		
		BasicConfigurator.configure();
		Logger logger = Logger.getLogger("Foo");
		
		// add a second appender - logs will also be added to the file logs.txt
		logger.addAppender(new org.apache.log4j.FileAppender(new org.apache.log4j.TTCCLayout(), "logs.txt"));
		
		logger.debug("Hello World");
		
		logger.warn("it's me");
		
		

	}

}
