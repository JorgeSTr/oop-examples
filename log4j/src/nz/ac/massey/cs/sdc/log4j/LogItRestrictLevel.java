package nz.ac.massey.cs.sdc.log4j;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Sample log application.
 * @author jens dietrich
 *
 */
public class LogItRestrictLevel {

	public static void main(String[] args) throws Exception {
		
		BasicConfigurator.configure();
		Logger logger = Logger.getLogger("Foo");
		
		// only log at level INFO and above - includes WARN, exclude DEBUG
		logger.setLevel(Level.INFO);

		logger.debug("Hello World");
		logger.warn("it's me");
	}

}
