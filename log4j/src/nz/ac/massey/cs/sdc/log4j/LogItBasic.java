package nz.ac.massey.cs.sdc.log4j;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Simple log application.
 * @author jens dietrich
 *
 */
public class LogItBasic {

	public static void main(String[] args) throws Exception {
		
		// configure logging
		BasicConfigurator.configure();
		
		// create a logger named "foo"
		Logger logger = Logger.getLogger("Foo");
		
		// log something
		logger.debug("Hello World");
		
		// log something else
		logger.warn("it's me");
		
		

	}

}
