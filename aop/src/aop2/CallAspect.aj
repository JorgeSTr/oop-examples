package aop2;


public aspect CallAspect {

	pointcut pc() : call(StringBuffer StringBuffer.append(int)) || call(StringBuffer StringBuffer.append(boolean));
	
    after() returning() : pc() { 
        System.out.println("aspect 2: method invoked!"); 
    } 
}
