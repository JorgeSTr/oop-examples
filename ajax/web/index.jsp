<%@page contentType="text/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        
        <script language="javascript" type="text/javascript">
            var xmlHttp = new XMLHttpRequest();
            function check4matchingNames() {
                
                var t = document.getElementById("name").value;
                if ((t == null) || (t == "")) return;
                var url = "validate.jsp?name=" + t;
                xmlHttp.open("GET", url, true);
                xmlHttp.onreadystatechange = updatePage;
                xmlHttp.send(null);
             }
             function updatePage() {
                if (xmlHttp.readyState == 4) {                    
                    var responseData = xmlHttp.responseText;
                    document.getElementById("matchingnames").value = responseData;
                }
             }
        </script>
        
        
    </head>
    <body>

    <h1>The AJAX First Name DB </h1>
    
    This example uses Ajax. The string entered is sent to the server, a list of matching names is compiled and is send back to the client 
        in the background. This example may not work with MSIE, this requires some changes in the code that initialises the XMLHttpRequest object.
 See also Brett McLaughlin's <a href="http://www-128.ibm.com/developerworks/web/library/wa-ajaxintro1.html" target="_blank">tutorial</a> for a discussion on this issue.
        
    <p/>
    <form name="form1">
        Enter a name here:<br/>
        <input type="text" name="name" id="name" value="" size="50" onkeyup="check4matchingNames();"/>
        <p/>
        <textarea name="matchingnames" id="matchingnames" cols="40" rows="5" readonly></textarea>
    </form>
    
    <p/><hr/><p/>
    <a href="index_jquery.jsp">version using jquery</a>
    
    </body>
</html>
