package test.nz.ac.massey.cs.sdc.guitest;

import org.junit.*;
import static org.junit.Assert.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static java.awt.event.KeyEvent.*;
import nz.ac.massey.cs.sdc.guitest.*;

/**
 * Automated test for the NotesManager. This test is rather basic and uses low level APIs (in particular 
 * java.awt.Robot). 
 * Users should not create key board and mouse events while tests run (you can only watch ..). 
 * There are some private methods to facilitate interaction with Robot, the code to convert characters to key code is by Adam Paynter
 * (http://stackoverflow.com/questions/1248510/convert-string-to-keyevents). 
 * Note that components to be tested are accessed directly using getters in the respective classes. GUI testing frameworks like Abbot use
 * matchers to look-up components by name and type in teh component tree (a bit like jquery works) to preserve better encapsulation. 
 * @author jens dietrich
 */


public class TestNoteManager {
	private NotesManager ui = null; 
	private Robot robot = null;
	
	@Before 
	public void openNoteManager() throws Exception {
		ui = new NotesManager();
		ui.setVisible(true);
		robot = new Robot();

	}
	// add this to have a chance to inspect the user interface before it closes
	// when the JVM instance used to run the test shuts down
	@After 
	public void waitABit() throws Exception {
		robot = null;
		Thread.sleep(1000);
	}
	@Test
	public void testAddOne() throws Exception {
		JButton btnAdd = ui.getBtnAddNote();
		btnAdd.doClick();	
		assertEquals(1,ui.getNoteEditors().length);
	}
	
	@Test
	public void testAddTwo() throws Exception {
		JButton btnAdd = ui.getBtnAddNote();
		btnAdd.doClick();	
		btnAdd.doClick();
		assertEquals(2,ui.getNoteEditors().length);
	}
	
	@Test
	public void testAddThree() throws Exception {
		JButton btnAdd = ui.getBtnAddNote();
		btnAdd.doClick();	
		btnAdd.doClick();
		btnAdd.doClick();
		assertEquals(3,ui.getNoteEditors().length);
	}
	
	@Test
	public void testAddThreeThenRemoveOne() throws Exception {
		JButton btnAdd = ui.getBtnAddNote();
		JButton btnRemove = ui.getBtnRemoveNote();
		btnAdd.doClick();	
		btnAdd.doClick();
		btnAdd.doClick();
		btnRemove.doClick();
		assertEquals(2,ui.getNoteEditors().length);
	}
	
	@Test
	public void testAddAndEditTitle() throws Exception {
		JButton btnAdd = ui.getBtnAddNote();
		btnAdd.doClick();	
		
		Thread.sleep(1000);
		
		NoteEditor editor = ui.getNoteEditors()[0];
		JTextField titleEditor = editor.getTitleEditor();
		
		clickOn(titleEditor);
		enterString("Hello world");
		
		// wait to give event delivery some time!
		Thread.sleep(1000);
		
		Note note = editor.getModel();
		assertEquals("Hello world",note.getTitle());
	}
	
	@Test
	public void testAddAndEditText() throws Exception {
		JButton btnAdd = ui.getBtnAddNote();
		btnAdd.doClick();	
		
		Thread.sleep(1000);
		
		NoteEditor editor = ui.getNoteEditors()[0];
		JTextPane textEditor = editor.getTextEditor();
		
		clickOn(textEditor);
		enterString("this is the body of the note");
		
		// wait to give event delivery some time!
		Thread.sleep(1000);
		
		Note note = editor.getModel();
		// note that enter will add a new line in a text pane
		assertEquals("this is the body of the note",note.getText().trim());
	}
	
	
	private void moveMouseTo(Component component) {
		Point p = component.getLocationOnScreen();
		robot.mouseMove(p.x+10, p.y+10);
	}
	
	private void clickOn(Component component) {
		component.requestFocus();
		//moveMouseTo(component);
		//robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mousePress(InputEvent.BUTTON1_MASK);
	}
	
	private void enterString(String s)
	{
		for (char c:s.toCharArray()) {
			type(c);
		}
		robot.keyPress(KeyEvent.VK_ENTER);
	}
	// code to convert characters to key code by Adam Paynter
	// see http://stackoverflow.com/questions/1248510/convert-string-to-keyevents
	public void type(char character) {
        switch (character) {
        case 'a': doType(VK_A); break;
        case 'b': doType(VK_B); break;
        case 'c': doType(VK_C); break;
        case 'd': doType(VK_D); break;
        case 'e': doType(VK_E); break;
        case 'f': doType(VK_F); break;
        case 'g': doType(VK_G); break;
        case 'h': doType(VK_H); break;
        case 'i': doType(VK_I); break;
        case 'j': doType(VK_J); break;
        case 'k': doType(VK_K); break;
        case 'l': doType(VK_L); break;
        case 'm': doType(VK_M); break;
        case 'n': doType(VK_N); break;
        case 'o': doType(VK_O); break;
        case 'p': doType(VK_P); break;
        case 'q': doType(VK_Q); break;
        case 'r': doType(VK_R); break;
        case 's': doType(VK_S); break;
        case 't': doType(VK_T); break;
        case 'u': doType(VK_U); break;
        case 'v': doType(VK_V); break;
        case 'w': doType(VK_W); break;
        case 'x': doType(VK_X); break;
        case 'y': doType(VK_Y); break;
        case 'z': doType(VK_Z); break;
        case 'A': doType(VK_SHIFT, VK_A); break;
        case 'B': doType(VK_SHIFT, VK_B); break;
        case 'C': doType(VK_SHIFT, VK_C); break;
        case 'D': doType(VK_SHIFT, VK_D); break;
        case 'E': doType(VK_SHIFT, VK_E); break;
        case 'F': doType(VK_SHIFT, VK_F); break;
        case 'G': doType(VK_SHIFT, VK_G); break;
        case 'H': doType(VK_SHIFT, VK_H); break;
        case 'I': doType(VK_SHIFT, VK_I); break;
        case 'J': doType(VK_SHIFT, VK_J); break;
        case 'K': doType(VK_SHIFT, VK_K); break;
        case 'L': doType(VK_SHIFT, VK_L); break;
        case 'M': doType(VK_SHIFT, VK_M); break;
        case 'N': doType(VK_SHIFT, VK_N); break;
        case 'O': doType(VK_SHIFT, VK_O); break;
        case 'P': doType(VK_SHIFT, VK_P); break;
        case 'Q': doType(VK_SHIFT, VK_Q); break;
        case 'R': doType(VK_SHIFT, VK_R); break;
        case 'S': doType(VK_SHIFT, VK_S); break;
        case 'T': doType(VK_SHIFT, VK_T); break;
        case 'U': doType(VK_SHIFT, VK_U); break;
        case 'V': doType(VK_SHIFT, VK_V); break;
        case 'W': doType(VK_SHIFT, VK_W); break;
        case 'X': doType(VK_SHIFT, VK_X); break;
        case 'Y': doType(VK_SHIFT, VK_Y); break;
        case 'Z': doType(VK_SHIFT, VK_Z); break;
        case '`': doType(VK_BACK_QUOTE); break;
        case '0': doType(VK_0); break;
        case '1': doType(VK_1); break;
        case '2': doType(VK_2); break;
        case '3': doType(VK_3); break;
        case '4': doType(VK_4); break;
        case '5': doType(VK_5); break;
        case '6': doType(VK_6); break;
        case '7': doType(VK_7); break;
        case '8': doType(VK_8); break;
        case '9': doType(VK_9); break;
        case '-': doType(VK_MINUS); break;
        case '=': doType(VK_EQUALS); break;
        case '~': doType(VK_SHIFT, VK_BACK_QUOTE); break;
        case '!': doType(VK_EXCLAMATION_MARK); break;
        case '@': doType(VK_AT); break;
        case '#': doType(VK_NUMBER_SIGN); break;
        case '$': doType(VK_DOLLAR); break;
        case '%': doType(VK_SHIFT, VK_5); break;
        case '^': doType(VK_CIRCUMFLEX); break;
        case '&': doType(VK_AMPERSAND); break;
        case '*': doType(VK_ASTERISK); break;
        case '(': doType(VK_LEFT_PARENTHESIS); break;
        case ')': doType(VK_RIGHT_PARENTHESIS); break;
        case '_': doType(VK_UNDERSCORE); break;
        case '+': doType(VK_PLUS); break;
        case '\t': doType(VK_TAB); break;
        case '\n': doType(VK_ENTER); break;
        case '[': doType(VK_OPEN_BRACKET); break;
        case ']': doType(VK_CLOSE_BRACKET); break;
        case '\\': doType(VK_BACK_SLASH); break;
        case '{': doType(VK_SHIFT, VK_OPEN_BRACKET); break;
        case '}': doType(VK_SHIFT, VK_CLOSE_BRACKET); break;
        case '|': doType(VK_SHIFT, VK_BACK_SLASH); break;
        case ';': doType(VK_SEMICOLON); break;
        case ':': doType(VK_COLON); break;
        case '\'': doType(VK_QUOTE); break;
        case '"': doType(VK_QUOTEDBL); break;
        case ',': doType(VK_COMMA); break;
        case '<': doType(VK_LESS); break;
        case '.': doType(VK_PERIOD); break;
        case '>': doType(VK_GREATER); break;
        case '/': doType(VK_SLASH); break;
        case '?': doType(VK_SHIFT, VK_SLASH); break;
        case ' ': doType(VK_SPACE); break;
        default:
                throw new IllegalArgumentException("Cannot type character " + character);
        }
    }

    private void doType(int... keyCodes) {
        doType(keyCodes, 0, keyCodes.length);
    }

    private void doType(int[] keyCodes, int offset, int length) {
        if (length == 0) {
                return;
        }

        robot.keyPress(keyCodes[offset]);
        doType(keyCodes, offset + 1, length - 1);
        robot.keyRelease(keyCodes[offset]);
    }

}

	
	

