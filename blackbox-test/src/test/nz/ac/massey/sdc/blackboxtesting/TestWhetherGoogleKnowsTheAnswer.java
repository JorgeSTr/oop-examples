package test.nz.ac.massey.sdc.blackboxtesting;

import static org.junit.Assert.*;
import java.net.URI;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Black box test - we test whether google knows the answer to 
 * "to life the universe and everything".
 * We test the web server from "the outside", see  mockobjects example
 * for an alternative (whitebox) technique. 
 * This test is rather low level, for production code usually a framework like
 * Selenium, HttpUnit, WebJUnit etc would be used that has the ability to parse
 * and check the DOM. 
 * @author jens dietrich
 */
public class TestWhetherGoogleKnowsTheAnswer {
	
	// proxy settings with default settings for Massey University NZ
	public static final boolean USE_PROXY = false;
	public static final String PROXY_HOST = "tur-cache1.massey.ac.nz"; 
	public static final int PROXY_PORT = 8080; 
	
			
	private static DefaultHttpClient httpClient = null;
	
	@BeforeClass
	public static void setupClass() throws Exception {
		httpClient = new DefaultHttpClient();
		
		System.out.println("try to change the proxy settings if this test times out");
		if (USE_PROXY) {
			HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
			httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		}
	}

    @Test(timeout=5000)
    public void testFormSubmission1() throws Exception {  	
		// create a google query url
		URIBuilder builder = new URIBuilder();
    	builder.setScheme("http").setHost("www.google.com").setPath("/search")
    	    .setParameter("q", "the answer to life the universe and everything");
    	URI uri = builder.build();
    	
    	// create and execute the request
    	HttpGet request = new HttpGet(uri);
    	HttpResponse response = httpClient.execute(request);
    	
    	// this string is the unparsed web page (=html source code)
    	String content = EntityUtils.toString(response.getEntity());
	    
	    // check whether the web page contains the expected answer
		assertTrue(content
				.indexOf("The answer to life the universe and everything = 42") > -1);
    }


}
