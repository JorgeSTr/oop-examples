package nz.ac.massey.cs.pp.listcomprehensions.guava;

import nz.ac.massey.cs.pp.listcomprehensions.Student;
import com.google.common.base.Predicate;
/**
 * Simple filter for Student instances based on the major property. 
 * @author jens dietrich
 */
public class FilterByMajor implements Predicate<Student> {
	private String major = null; 
	public FilterByMajor(String major) {
		super();
		this.major = major;
	}
	@Override
	public boolean apply(Student student) {
		return student.getMajor().equals(major);
	}

}
