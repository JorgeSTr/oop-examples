package nz.ac.massey.cs.pp.listcomprehensions.guava;


import java.util.Iterator;
import static com.google.common.collect.Iterators.*;
import nz.ac.massey.cs.pp.listcomprehensions.*;

/**
 * Filter and convert objects using the list comprehensions in Guava.
 * This example uses lazy initialisation.
 * @author jens dietrich
 */

public class PrintSelectedStudentNames2 {

	public static void main(String[] args) {
		
		Iterator<Student> students = StudentFactory.getStudents().iterator();
		Iterator<Student> toms = filter(students, new FilterByFirstName("Tom"));
		Iterator<Student> seToms = filter(toms,new FilterByMajor("Software Engineering"));
		Iterator<String> names = transform(seToms,new GetFullName());
		
		while (names.hasNext()) {
			System.out.println(names.next());
		}
	}

}
