package nz.ac.massey.cs.pp.listcomprehensions.guava;

import java.util.Collection;
import static com.google.common.collect.Collections2.*;
import nz.ac.massey.cs.pp.listcomprehensions.*;

/**
 * Filter and convert objects using the list comprehensions in Guava.
 * This example uses eager initialisation.
 * @author jens dietrich
 */

public class PrintSelectedStudentNames1 {

	public static void main(String[] args) {
		
		Collection<Student> students = StudentFactory.getStudents();
		Collection<Student> toms = filter(students, new FilterByFirstName("Tom"));
		Collection<Student> seToms = filter(toms,new FilterByMajor("Software Engineering"));
		Collection<String> names = transform(seToms,new GetFullName());
		
		for (String name:names) {
			System.out.println(name);
		}
	}

}
